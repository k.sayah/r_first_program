##########################################################
#
# First R program
#
# Antoine Lamer
#
# 22/08/08
#
# Objectives :
# Load two csv files : patient.csv and hospital_stay.csv
# Describe the two files
# Merge patients and hospital stays
# Transform some dates
# Compute new variables
# Explore the dataset
# Display a graphic
#
##########################################################



##########################################################
#
# Variables names :
# - no special character except _
# - lowercase
# - no useless words (e.g., dataset)
# - avoid number to prefer a description of the state of the object
# (e.g. patient_deduplicated)
#
##########################################################


# Path
# Update the path with your own directory
##########################################################
path_data = ""

# Read the files
patient = read.csv2(file.path(path_data, "patient.csv"))
hospital_stay = read.csv2(file.path(path_data, "hospital_stay.csv"))

# If correctly loaded, the datasets are displayed in the panel Environment

# Describe the dataset
##########################################################

# patient
str(patient)
head(patient)

# stay
str(hospital_stay)
head(hospital_stay, n = 10)
tail(hospital_stay, 5)

# Manipulate the dataset
##########################################################

# 1. Merge the two dataframes

patient_hospital_stay = merge(patient, hospital_stay, by = "patient_id")
str(patient_stay)

# 2. Reformate date

# Display the documentation of the function strptime
?strptime

patient_stay$birth_date_format = strptime(patient_stay$birth_date, format = "%Y%m%d", tz = "CET")
patient_stay$admission_date = strptime(patient_stay$admission_date, format = "%Y%m%d", tz = "CET")
patient_stay$discharge_date = strptime(patient_stay$discharge_date, format = "%Y%m%d", tz = "CET")

patient_hospital_stay$birth_date_format = strptime(patient_hospital_stay$birth_date, format = "%Y%m%d", tz = "CET")


patient_hospital_stay[, c("birth_date", "birth_date_format")]
# 3. Compute the difference between birth date and admission date
# and the duration of the hospital stay

patient_stay$age_at_admission = round(difftime(patient_stay$admission_date, 
                                               patient_stay$birth_date, 
                                               units = "days") / 365)

patient_stay$stay_duration = difftime(patient_stay$discharge_date, 
                                      patient_stay$admission_date, 
                                      units = "days")

str(patient_stay)

patient_stay$stay_duration = as.numeric(patient_stay$stay_duration)
str(patient_stay)

# Explore the dataset
##########################################################

# Number of patients by sex categories
table(patient_stay$sex)

# Number of patients by sex categories and hospital
table(patient_stay$sex, patient_stay$hospital)

# Get the percentage in column with the function prop.table
table_sex = table(patient_stay$sex)
prop.table(table_sex, 2)

# Visualization with functions
# from the gaphics pakage
##########################################################

# Display a barplot with the number of patients by sex
# with the function barplot and table_sex

# Update the barplot in 
# adding the title "Number of patients per sex"
# changing the color to #2B3B5E


# Display a boxplot with patients age, with the title "Age at admission"

# Display a histogram with the stay duration 
# with the title "Duration of hospital stay"
# the title of the y axis : "Number of stay"

